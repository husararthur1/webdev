const addStudent = $(".add-student-container");
const removeWarning = $(".remove-student-warning-container");
const btnCloseAddStudent = $("#close-student-form");
const btnAddNewStudent = $("#add-new-student-button");
const overlay = $(".overlay");
const pages = $(".pagination .a-pag");

const pagPrevious = $(".pagination .a-pag-previous");
const pagNext = $(".pagination a-pag-next");

let studentTable = $("#students-table");
const warningMsg = $("#warning-message");
btnAddNewStudent.on("click", function () {
  currentContext = "add";
  switchToCurrContext(currentContext);
  overlay.toggleClass("hidden");
  addStudent.toggleClass("hidden");
});

btnCloseAddStudent.on("click", function (e) {
  e.preventDefault(); // Prevents the default behavior of the close button
  overlay.addClass("hidden");
  addStudent.addClass("hidden"); // Use addClass to hide the element
});

let currentContext = "add";

//Fetching data from ADD Student
const btnAddStudentOk = $("#add-student-ok-button");
const btnAddStudentCreate = $("#add-student-create-button");

const mainCheckBox = $("#main-checkbox");

let studentData = {
  group: " ",
  firstName: " ",
  lastName: " ",
  gender: " ",
  birthDay: {
    day: " ",
    month: " ",
    year: " ",
  },
  status: "online",
};

class Student {
  static count = 0;

  constructor(data) {
    this.group = data.group || "";
    this.firstName = data.firstName || "";
    this.lastName = data.lastName || "";
    this.gender = data.gender || "";
    this.birthDay = {
      year: data.birthDay.year || "",
      month: data.birthDay.month || "",
      day: data.birthDay.day || "",
    };
    this.status = data.status || "offline";
    this.id = this.generateStudentId();
    Student.count++;
  }

  generateStudentId() {
    const { year, month, day } = this.birthDay;
    const count = Student.count.toString().padStart(3, "0");

    return `${this.lastName.substring(0, 3).toUpperCase()}${this.firstName
      .substring(0, 1)
      .toUpperCase()}${year}${month}${day}${count}`;
  }
}
let students = [];
students.push(new Student(studentData));
const tbody = $("tbody");
let stToEdit = students[0];
students.pop();
const clearFormFields = function () {
  $("#group-select").val("");
  $("#first-name-input").val("");
  $("#last-name-input").val("");
  $("#gender-select").val("");
  $("#birthday-input").val("");
};

const initStudent = function () {
  const std = studentData;
  std.group = $("#group-select").val();
  std.firstName = $("#first-name-input").val();
  std.lastName = $("#last-name-input").val();
  std.gender = $("#gender-select").val();

  const [year, month, day] = $("#birthday-input").val().split("-");
  std.birthDay["year"] = year;
  std.birthDay["month"] = month;
  std.birthDay["day"] = day;
  return std;
};

const fillEditForm = function (studentData) {
  clearFormFields();

  $("#group-select").val(studentData.group);
  $("#first-name-input").val(studentData.firstName);
  $("#last-name-input").val(studentData.lastName);
  $("#gender-select").val(studentData.gender);
  $("#birthday-input").val(
    `${
      studentData.birthDay["year"] -
      studentData.birthDay["month"] -
      studentData.birthDay["day"]
    }`
  );
};

const readStudentFromRow = function (element) {
  const std = studentData;

  std.group = $(element).find("td:nth-child(2)").text();
  const name = $(element).find("td:nth-child(3)").text();
  [first, last] = name.split(" ");
  std.firstName = first;
  std.lastName = last;
  std.gender = $(element).find("td:nth-child(4)").text();

  const [day, month, year] = $(element)
    .find("td:nth-child(5)")
    .text()
    .split(".");
  std.birthDay.year = year;
  std.birthDay.month = month;
  std.birthDay.day = day;
  // std.status = $(element).find("td:nth-child(6)");
  return std;
};

const editRow = function (student) {
  $(rowToEdit).find("td:nth-child(2)").text(student.group);
  $(rowToEdit)
    .find("td:nth-child(3)")
    .text(`${student.firstName} ${student.lastName}`);
  $(rowToEdit).find("td:nth-child(4)").text(student.gender);

  $(rowToEdit)
    .find("td:nth-child(5)")
    .text(
      `${student.birthDay.day}.${student.birthDay.month}.${student.birthDay.year}`
    );

  $(rowToEdit).find("td:nth-child(6)").toggleClass(`${student.status}`);
  for (let i = 0; i < students.length; i++) {
    if (students[i].id === student.id) {
      students[i] = student;
    }
  }
};

function checkStudentData(student) {}

function addStudentToTable(student) {
  const tr = $("<tr></tr>");
  const checkboxCell = $("<td></td>");
  const checkboxInput = $("<input>");
  $(checkboxInput).attr("type", "checkbox");
  $(checkboxInput).addClass("secondary-checkbox");
  $(checkboxCell).append(checkboxInput);
  $(tr).append(checkboxCell);

  const groupCell = $("<td></td>");
  $(groupCell).text(student.group);
  $(tr).append(groupCell);

  const nameCell = $("<td></td>");
  $(nameCell).text(`${student.firstName} ${student.lastName}`);
  $(tr).append(nameCell);

  const genderCell = $("<td></td>");
  $(genderCell).text(student.gender);
  $(tr).append(genderCell);

  const birthDayCell = $("<td></td>");
  $(birthDayCell).text(
    `${student.birthDay.day}.${student.birthDay.month}.${student.birthDay.year}`
  );
  $(tr).append(birthDayCell);

  const statusCell = $("<td></td>");
  const statusC = $("<div></div>");
  $(statusC).addClass("student-status online");
  $(statusCell).append(statusC);
  $(tr).append(statusCell);

  const optionButtonsCell = $("<td></td>");
  const optionButtons = $("<div></div>");
  $(optionButtons).addClass("option-buttons");

  const editButtonC = $("<div></div>");
  $(editButtonC).addClass("edit-student");
  const editButton = $("<button></button>");
  $(editButton).addClass("edit-student-button");
  $(editButton).addClass("btn");
  $(editButton).addClass("btn-outline-success");
  const editImg = $("<img>");
  $(editImg).attr("src", "img/edit.png");
  $(editImg).attr("style", "width: 10px; height: 10px");
  $(editImg).attr("alt", "edit");

  $(editButton).append(editImg);
  $(editButtonC).append(editButton);
  $(optionButtons).append(editButtonC);

  const removeButtonC = $("<div></div>");
  $(removeButtonC).addClass("remove-student");
  const removeButton = $("<button></button>");
  $(removeButton).addClass("remove-student-button");
  $(removeButton).addClass("btn");
  $(removeButton).addClass("btn-outline-danger");
  const removeImg = $("<img>");
  $(removeImg).attr("src", "img/close.png");
  $(removeImg).attr("style", "width: 10px; height: 10px");
  $(removeImg).attr("alt", "close");

  $(removeButton).append(removeImg);
  $(removeButtonC).append(removeButton);
  $(optionButtons).append(removeButtonC);
  $(optionButtonsCell).append(optionButtons);
  $(tr).attr("id", student.id);
  $(tr).append(optionButtonsCell);
  $(tbody).append(tr);
}

function addNewStudent() {
  studentData = initStudent();
  const student = new Student(studentData);
  students.push(student);
  // if (!checkStudentData(student)) return;
  addStudentToTable(student);
  $(overlay).toggleClass("hidden");
  $(addStudent).toggleClass("hidden");
  clearFormFields();
}

btnAddStudentOk.click(function (e) {
  e.preventDefault();
  if (currentContext === "add") {
    addNewStudent();
  } else if (currentContext === "edit") {
    editRow(stToEdit);
  }
  sendDataToServer();
});

btnAddStudentCreate.click(addNewStudent);

const btnRemoveStudent = $(".remove-student-button");
const btnConfirmRemoval = $("#confirm-remove-button");
const btnCancelRemoval = $("#cancel-remove-button");
const btnCloseWarning = $("#warning-close-button");

let rowToRemove, rowToEdit;
const switchToCurrContext = function () {
  const info = $("#add-student-header-caption");
  if (currentContext === "edit") {
    info.text("Edit Student");
    btnAddStudentCreate.text("Save");
  } else {
    info.text("Add Student");
    btnAddStudentCreate.text("Create");
  }
};

$(studentTable).on("click", function (e) {
  if (e.target && $(e.target).hasClass("remove-student-button")) {
    rowToRemove = $(e.target).closest("tr");

    warningMsg.text(
      `Are you sure you want to remove \n${$(rowToRemove)
        .find("td:nth-child(3)")
        .text()}?`
    );
    btnConfirmRemoval.on("click", function () {
      const id = rowToRemove.attr("id");
      const stRemove = students.find((st) => st.id === id);

      students.splice(students.indexOf(stRemove), 1);
      rowToRemove.remove();
      overlay.addClass("hidden");
      removeWarning.addClass("hidden");
    });
    overlay.toggleClass("hidden");
    removeWarning.toggleClass("hidden");
  }

  if ($(e.target).is("#main-checkbox")) {
    $(".secondary-checkbox").prop("checked", e.target.checked);
  }
  if (e.target && $(e.target).hasClass("edit-student-button")) {
    rowToEdit = $(e.target).closest("tr");
    overlay.toggleClass("hidden");
    addStudent.toggleClass("hidden");
    currentContext = "edit";
    switchToCurrContext();
    stToEdit = students.find((st) => st.id === rowToEdit.attr("id"));
    fillEditForm(stToEdit);
  }
});
btnCancelRemoval.on("click", function () {
  overlay.addClass("hidden");
  removeWarning.addClass("hidden");
});

btnCloseWarning.on("click", function () {
  overlay.addClass("hidden");
  removeWarning.addClass("hidden");
});

// pagNext.on("click", function () {
//   let current = document.querySelector(".active");
//   let currentIndex = Array.from(pages).indexOf(current);

//   if (currentIndex === pages.length - 1) {
//     pages[0].classList.add("active");
//   } else {
//     pages[currentIndex + 1].classList.add("active");
//   }

//   if (current) {
//     current.classList.remove("active");
//   }
// });
pagPrevious.on("click", function () {
  let current = $(".active");
  let currentIndex = pages.index(current);

  if (currentIndex <= 0) {
    pages.eq(pages.length - 1).addClass("active");
  } else {
    pages.eq(currentIndex - 1).addClass("active");
  }

  if (current) {
    current.removeClass("active");
  }
});

pagNext.on("click", function () {
  let current = $(".active");
  let currentIndex = pages.index(current);

  if (currentIndex === pages.length - 1) {
    pages.eq(0).addClass("active");
  } else {
    pages.eq(currentIndex + 1).addClass("active");
  }

  if (current) {
    current.removeClass("active");
  }
});
pages.each(function () {
  $(this).on("click", function () {
    $(".active").removeClass("active");
    $(this).addClass("active");
  });
});

function sendDataToServer() {
  const studentsJSON = JSON.stringify(students);
  console.log(studentsJSON);

  $.ajax({
    type: "POST",
    url: "https://jsonplaceholder.typicode.com/posts",
    data: studentsJSON,
    success: function (response) {
      console.log("Sent.");
    },
    error: function (xhr, status, error) {
      console.error("Failed. Error :", error);
    },
  });
}
