const addStudent = document.querySelector(".add-student-container");
const removeWarning = document.querySelector(
  ".remove-student-warning-container"
);
const btnCloseAddStudent = document.getElementById("close-student-form");
const btnAddNewStudent = document.getElementById("add-new-student-button");
// const btnRemoveStudent = document.querySelectorAll(".remove-student-button");
// const btnConfirmRemoval = document.getElementById("confirm-remove-button");
// const btnCancelRemoval = document.getElementById("cancel-remove-button");
// const btnCloseWarning = document.getElementById("warning-close-button");
const overlay = document.querySelector(".overlay");
const pages = document.querySelector(".pagination").querySelectorAll(".a-pag");

const pagPrevious = document
  .querySelector(".pagination")
  .querySelector(".a-pag-previous");
const pagNext = document
  .querySelector(".pagination")
  .querySelector(".a-pag-next");

let studentTable = document.getElementById("students-table");
const warningMsg = document.getElementById("warning-message");
btnAddNewStudent.addEventListener("click", function () {
  overlay.classList.toggle("hidden");
  addStudent.classList.toggle("hidden");
});

btnCloseAddStudent.addEventListener("click", function () {
  addStudent.classList.add("hidden");
  overlay.classList.toggle("hidden");
});

//Fetching data from ADD Student
const btnAddStudentOk = document.getElementById("add-student-ok-button");
const btnAddStudentCreate = document.getElementById(
  "add-student-create-button"
);

const student = {
  group: " ",
  firstName: " ",
  lastName: " ",
  gender: " ",
  birthDay: {
    day: " ",
    month: " ",
    year: " ",
  },
  status: "online",
};
const tbody = document.querySelector("tbody");
function checkStudentData(student) {}

function addStudentToTable(student) {
  const tr = document.createElement("tr");
  const checkboxCell = document.createElement("td");
  const checkboxInput = document.createElement("input");
  checkboxInput.type = "checkbox";
  checkboxCell.appendChild(checkboxInput);
  tr.appendChild(checkboxCell);

  const groupCell = document.createElement("td");
  groupCell.textContent = student.group;
  tr.appendChild(groupCell);

  const nameCell = document.createElement("td");
  nameCell.textContent = `${student.firstName} ${student.lastName}`;
  tr.appendChild(nameCell);

  const genderCell = document.createElement("td");
  genderCell.textContent = student.gender;
  tr.appendChild(genderCell);

  const birthDayCell = document.createElement("td");
  birthDayCell.textContent = `${student.birthDay.day}.${student.birthDay.month}.${student.birthDay.year}`;
  tr.appendChild(birthDayCell);

  const statusCell = document.createElement("td");
  const statusC = document.createElement("div");
  statusC.classList.add("student-status", "online");
  statusCell.appendChild(statusC);
  tr.appendChild(statusCell);

  const optionButtonsCell = document.createElement("td");
  const optionButtons = document.createElement("div");
  optionButtons.classList.add("option-buttons");

  const editButtonC = document.createElement("div");
  editButtonC.classList.add("edit-student");
  const editButton = document.createElement("button");
  editButton.classList.add("edit-student-button");
  const editImg = document.createElement("img");
  editImg.setAttribute("src", "img/edit.png");
  editImg.setAttribute("style", "width: 10px; height: 10px");
  editImg.setAttribute("alt", "edit");

  editButton.appendChild(editImg);
  editButtonC.appendChild(editButton);
  optionButtons.append(editButtonC);

  const removeButtonC = document.createElement("div");
  removeButtonC.classList.add("remove-student");
  const removeButton = document.createElement("button");
  removeButton.classList.add("remove-student-button");
  const removeImg = document.createElement("img");
  removeImg.setAttribute("src", "img/close.png");
  removeImg.setAttribute("style", "width: 10px; height: 10px");
  removeImg.setAttribute("alt", "close");

  removeButton.appendChild(removeImg);
  removeButtonC.appendChild(removeButton);
  optionButtons.append(removeButtonC);
  optionButtonsCell.appendChild(optionButtons);

  tr.appendChild(optionButtonsCell);
  tbody.appendChild(tr);
}

btnAddStudentOk.addEventListener("click", function (e) {
  e.preventDefault();
  student.group = document.getElementById("group-select").value;
  student.firstName = document.getElementById("first-name-input").value;
  student.lastName = document.getElementById("last-name-input").value;
  student.gender = document.getElementById("gender-select").value;

  const [year, month, day] = document
    .getElementById("birthday-input")
    .value.split("-");
  student.birthDay["year"] = year;
  student.birthDay["month"] = month;
  student.birthDay["day"] = day;
  console.log(student);
  // if (!checkStudentData(student)) return;
  addStudentToTable(student);
  overlay.classList.toggle("hidden");
  addStudent.classList.toggle("hidden");
});

btnAddStudentCreate.addEventListener("click", function () {
  student.group = document.getElementById("group-select").value;
  student.firstName = document.getElementById("first-name-input").value;
  student.lastName = document.getElementById("last-name-input").value;
  student.gender = document.getElementById("gender-select").value;

  const [year, month, day] = document
    .getElementById("birthday-input")
    .value.split("-");
  student.birthDay["year"] = year;
  student.birthDay["month"] = month;
  student.birthDay["day"] = day;
  console.log(student);
  // if (!checkStudentData(student)) return;
  addStudentToTable(student);
  overlay.classList.toggle("hidden");
  addStudent.classList.toggle("hidden");
});

const btnRemoveStudent = document.querySelectorAll(".remove-student-button");
const btnConfirmRemoval = document.getElementById("confirm-remove-button");
const btnCancelRemoval = document.getElementById("cancel-remove-button");
const btnCloseWarning = document.getElementById("warning-close-button");

let rowToRemove;

studentTable.addEventListener("click", function (e) {
  if (e.target && e.target.matches(".remove-student-button")) {
    rowToRemove = e.target.closest("tr");
    warningMsg.innerText = `Are you sure you want to remove ${
      rowToRemove.querySelector("td:nth-child(3)").textContent
    }?`;
    btnConfirmRemoval.addEventListener("click", function () {
      rowToRemove.remove();
      overlay.classList.add("hidden");
      removeWarning.classList.add("hidden");
    });
    overlay.classList.toggle("hidden");
    removeWarning.classList.toggle("hidden");
  }
});

// btnRemoveStudent.forEach((removeButton) =>
//   removeButton.addEventListener("click", () => {
//     rowToRemove = removeButton.closest("tr");
//     warningMsg.innerText = `Are you sure you want to remove ${
//       rowToRemove.querySelector("td:nth-child(3)").textContent
//     }?`;
//     //Approve removal
//     btnConfirmRemoval.addEventListener("click", function () {
//       rowToRemove.remove();
//       overlay.classList.add("hidden");
//       removeWarning.classList.add("hidden");
//     });

//     overlay.classList.toggle("hidden");
//     removeWarning.classList.toggle("hidden");
//   })
// );

//Cancel Removal
btnCancelRemoval.addEventListener("click", function () {
  overlay.classList.add("hidden");
  removeWarning.classList.add("hidden");
});

btnCloseWarning.addEventListener("click", function () {
  overlay.classList.add("hidden");
  removeWarning.classList.add("hidden");
});

pagPrevious.addEventListener("click", function () {
  let current = document.querySelector(".active");
  let currentIndex = Array.from(pages).indexOf(current);

  if (currentIndex <= 0) {
    pages[pages.length - 1].classList.add("active");
  } else {
    pages[currentIndex - 1].classList.add("active");
  }

  if (current) {
    current.classList.remove("active");
  }
});

pagNext.addEventListener("click", function () {
  let current = document.querySelector(".active");
  let currentIndex = Array.from(pages).indexOf(current);

  if (currentIndex === pages.length - 1) {
    pages[0].classList.add("active");
  } else {
    pages[currentIndex + 1].classList.add("active");
  }

  if (current) {
    current.classList.remove("active");
  }
});

pages.forEach((page) => {
  page.addEventListener("click", () => {
    let current = document.querySelector(".active");

    if (current) {
      current.classList.remove("active");
    }

    page.classList.add("active");
  });
});
