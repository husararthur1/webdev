const socket = io("http://localhost:3000/");
const boardCont = document.querySelector(".boards-cont");
const btnChangeTask = document.querySelector("#btn-change-task");
const btnCancelChange = document.querySelector("#btn-cancel-change");
const boardSelect = document.getElementById("board-select");
const taskWizard = document.querySelector(".task-wizard-cont");
const taskNameInput = document.getElementById("task-name-input");
const taskDateInput = document.getElementById("task-date-input");
const taskDescInput = document.getElementById("task-description-input");
let currentTask = null;
socket.emit("fetch tasks", User);
const taskList = [];
socket.on("tasks", (tasks) => {
  console.log(tasks);
  tasks.forEach((task) => {
    console.log(task);
    outputTask(task);
  });
});
function showTaskWizard(task = null) {
  if (User.loggedIn == false) {
    alert("Logg in first");
    return;
  }
  if (task) {
    boardSelect.value = task.board;
    taskNameInput.value = task.name;
    taskDateInput.value = task.date;
    taskDescInput.value = task.description;
    currentTask = task;
  } else {
    boardSelect.value = "ToDo";
    taskNameInput.value = "";
    taskDateInput.value = "";
    taskDescInput.value = "";
    currentTask = null;
  }
  boardCont.classList.add("hidden");
  taskWizard.classList.remove("hidden");
}

function hideTaskWizard() {
  boardCont.classList.remove("hidden");
  taskWizard.classList.add("hidden");
}

function createTaskElement(task) {
  const taskElement = document.createElement("div");
  taskElement.classList.add("task-wrapper");
  const taskName = document.createElement("div");
  taskName.classList.add("task-wrapper-conent");
  const taskDate = document.createElement("div");
  taskDate.classList.add("task-date");
  taskName.textContent = task.name;
  taskDate.textContent = parseDate(task.date);

  taskElement.dataset.board = task.board;
  taskElement.dataset.name = task.name;
  taskElement.dataset.date = task.date;
  taskElement.dataset.description = task.description;
  taskElement.dataset.taskId = task._id;
  taskElement.append(taskName);
  taskElement.append(taskDate);

  taskElement.addEventListener("click", () => showTaskWizard(task));
  return taskElement;
}

function parseDate(date) {
  const dateParts = date.split("-");
  return `${dateParts[2]}.${dateParts[1]}`;
}
document.querySelectorAll(".btn-add-task").forEach((btnAddTask) => {
  btnAddTask.addEventListener("click", () => {
    showTaskWizard();
  });
});

btnChangeTask.addEventListener("click", () => {
  const task = {
    board: boardSelect.value,
    name: taskNameInput.value,
    date: taskDateInput.value,
    description: taskDescInput.value,
    _id: currentTask ? currentTask._id : null,
  };
  console.log(task);
  socket.emit("task change", task, User);
  socket.on("task changed", () => {
    outputTask(task);
    btnCancelChange.click();
  });
});

function outputTask(task) {
  if (currentTask) {
    const oldBoard = document.querySelector(
      `.task-board[data-board="${currentTask.board}"] .task-list`
    );
    oldBoard.removeChild(currentTask.element);
  }
  const newBoard = document.querySelector(
    `.task-board[data-board="${task.board}"] .task-list`
  );
  const taskElement = createTaskElement(task);
  newBoard.appendChild(taskElement);
  task.element = taskElement;
  //currentTask = task;
}

btnCancelChange.addEventListener("click", () => {
  hideTaskWizard();
});
