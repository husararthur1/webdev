import mongodb from "mongodb";
import { ObjectId } from "mongodb";
const { MongoClient } = mongodb;
const dbUrl =
  "mongodb+srv://arturhuszar:ysdBBtbFG44LHH2c@cluster0.n39p4xb.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0";
const dbName = "chats";
let db;
const mongoConnect = (dbUrl, dbName, callback) => {
  MongoClient.connect(dbUrl)
    .then((client) => {
      console.log("Connected to MongoDB");
      db = client.db(dbName);
      callback(db, client);
    })
    .catch((err) => console.log(err));
};
const mongoDisconnect = (client) => {
  client
    .close()
    .then(() => {
      console.log("Disconnected from MongoDB");
    })
    .catch((err) => console.log(err));
};
//const createRoom = async (roomname, users) => {
//  try {
//    const resp = await findRoom(roomname);
//    console.log(`Response ${resp.room._id}`);
//    //const room = resp.room;
//    users.forEach(async (user) => {
//      await checkUserChats(resp.room._id, user.username);
//    });
//    return resp;
//  } catch (error) {
//    console.error("Error joining room:", error);
//    throw error;
//  }
//};

const createRoom = (roomname, users) => {
  return findRoom(roomname)
    .then((resp) => {
      console.log(`Response ${resp.room._id}`);
      const promises = users.map((user) =>
        checkUserChats(resp.room._id, user.username)
      );
      return Promise.all(promises).then(() => resp);
    })
    .catch((error) => {
      console.error("Error joining room:", error);
      throw error;
    });
};

const findRoom = async (_roomname) => {
  const foundRoom = await db
    .collection("rooms")
    .findOne({ roomname: _roomname });

  if (foundRoom) {
    return { res: "found room", room: foundRoom };
  }

  await db
    .collection("rooms")
    .insertOne({ _id: new ObjectId(), roomname: _roomname, messages: [] });
  const newRoom = await db.collection("rooms").findOne({ roomname: _roomname });
  return { res: "new room", room: newRoom };
};

const joinroom = async (room) => {
  try {
    const chatRoom = await db.collection("rooms").findOne({ roomname: room });
    const chathistory = chatRoom ? chatRoom.messages : [];
    return chathistory;
  } catch (err) {
    throw "DB: Couldnt get the messages from " + room;
  }
};

const checkUserChats = async (_roomId, _username) => {
  console.log(`Checking roomId with ${_roomId}`);

  const _user = await db
    .collection("users")
    .findOne(
      { username: _username, "chats.roomId": _roomId },
      { projection: { "chats.roomId": 1 } }
    );
  if (!_user) {
    const find = await db.collection("users").findOne({ username: _username });
    if (!find) {
      console.log(`User ${_username} not found`);
      return;
    }
    // const room = await db.collection("rooms").findOne({ _id: _roomId });
    console.log(`adding ${_username} to new room ${_roomId}`);
    const result = await db
      .collection("users")
      .updateOne(
        { username: _username },
        { $push: { chats: { roomId: _roomId } } }
      );
  } else {
    console.log("User found");
  }
  //console.log(await db.collection("users").find({}).toArray());
};

const retrieveChatRooms = async (user) => {
  const _user = await db
    .collection("users")
    .findOne({ username: user.username });
  if (!user) {
    console.log(`Chat Rooms: No user ${user.username} found`);
  }
  //console.log(_user);
  //console.log(_user.chats);
  //const chats = _user.chats;

  if (_user.chats.length == 0) {
    console.log("_user.chats is empty");
    return [];
  }
  const roomIds = await _user.chats.map((chat) => chat.roomId);
  const rooms = await db
    .collection("rooms")
    .find({ _id: { $in: roomIds } })
    .toArray();
  if (rooms.length == 0) {
    console.log("rooms are empty");
    return [];
  }
  return rooms;
};

const retrieveChatMessages = async (roomname) => {
  try {
    const chatRoom = await db
      .collection("rooms")
      .findOne({ roomname: roomname });
    if (!chatRoom) {
      return [];
    }
    return chatRoom.messages.find({}).toArray();
  } catch (err) {
    console.error(
      `Error retrieving and sorting messages of chat room ${room.roomname}:`,
      err
    );
    return [];
  }
};
const getUsers = async (_roomname) => {
  const room = await db.collection("rooms").findOne({ roomname: _roomname });
  if (!room) {
    return { resp: "err", msg: `couldnt find ${_roomname}` };
  }
  const _users = await db
    .collection("users")
    .find({ "chats.roomId": room._id })
    .toArray();
  if (_users.length == 0) {
    return { resp: "err", msg: `no users found in ${_roomname}` };
  }
  return { resp: "ok", msg: "users found", users: _users };
};

const getAvailableUsers = async (_roomname) => {
  const room = await db.collection("rooms").findOne({ roomname: _roomname });
  let roomID = room ? room._id : " ";

  const usersInRoom = await db
    .collection("users")
    .find({ "chats.roomId": roomID })
    .toArray();

  const usersInRoomIds = usersInRoom.map((user) => user._id);

  const availableUsers = await db
    .collection("users")
    .find({ _id: { $nin: usersInRoomIds } })
    .toArray();
  if (availableUsers.length == 0) {
    return [];
  }
  return availableUsers;
};
const addMessage = async (room, message, time, user) => {
  try {
    const findRoom = db.collection("rooms").findOne({ _id: room._id });
    if (!findRoom) {
      return { res: "error", msg: `DB Error: No room ${room.roomname} found` };
    }
    await db.collection("rooms").updateOne(
      { _id: room._id },
      {
        $push: {
          messages: {
            content: message,
            userId: user._id,
            username: user.username,
            timestamp: time,
          },
        },
      }
    );
    return {
      res: "success",
      msg: `DB: Message ${message} added to ${room.roomname} Room`,
    };
  } catch (error) {
    console.error("DB: Error Sending room:", error);
    throw error;
  }
};

const checkForNewUsers = (usersData) => {
  usersData.users.forEach((user) => {
    db.collection("users")
      .findOne({ username: user.username, password: user.password })
      .then((existingUser) => {
        if (!existingUser) {
          user.chats = [];
          //console.log("new user " + user);
          db.collection("users").insertOne(user);
        }
      })
      .catch((err) => {
        console.log(
          `Error checking for user ${user.username} in the database:`,
          err
        );
      });
  });
};
const loginUser = (user) => {
  return db
    .collection("users")
    .findOne({ username: user.username })
    .then((login) => {
      if (!login) {
        return { res: "error", msg: `${user.username} wrong login` };
      }
      return db
        .collection("users")
        .findOne({ username: user.username, password: user.password })
        .then((logged) => {
          if (logged) {
            return {
              res: "ok",
              msg: `DB: ${user.username} logged in`,
              userId: logged._id,
            };
          } else {
            return { res: "error", msg: ` ${user.username} wrong password` };
          }
        });
    })
    .catch((error) => {
      console.error("Error occurred during login:", error);
      return { res: "error", msg: "An error occurred during login" };
    });
};
const registerUser = (user) => {
  return db
    .collection("users")
    .findOne({ username: user.username })
    .then((login) => {
      if (login) {
        return { res: "error", msg: `Username ${user.username} is taken` };
      }
      db.collection("users")
        .insertOne({
          username: user.username,
          password: user.password,
          chats: [],
        })
        .then((registered) => {
          if (registered) {
            return {
              res: "ok",
              msg: `DB: ${user.username} registered`,
              userId: registered._id,
            };
          }
        });
    })
    .catch((error) => {
      console.error("Error occurred during login:", error);
      return { res: "error", msg: "An error occurred during login" };
    });
};

const fetchTasks = async () => {
  return await db.collection("tasks").find({}).toArray();
};

// const updateTasks = async (task) => {
//   const foundTask = await db
//     .collection("tasks")
//     .find({ board: task.board, name: task.name });
//   if (foundTask) {
//     const resp = await db.collection("tasks").updateOne(
//       { _id: foundTask._id },
//       {
//         board: foundTask.board,
//         description: foundTask.description,
//         date: foundTask.date,
//       }
//     );
//     if(resp.modifiedCount > 0) {
//       return { resp: "ok", msg: `updated ${foundTask.name} task` };
//     }
//   } else {
//     try {
//       await db.collection("tasks").insertOne({
//         board: task.board,
//         name: task.name,
//         date: task.date,
//         description: task.description,
//       });
//     } catch (err) {
//       return { resp: "error", msg: "Error inserting task: " + err };
//     }
//   }
// };
//
const updateTasks = (task) => {
  const taskId = task._id;
  return db
    .collection("tasks")
    .findOne({ _id: taskId })
    .then((foundTask) => {
      if (foundTask) {
        return db
          .collection("tasks")
          .updateOne(
            { _id: foundTask._id },
            {
              board: foundTask.board,
              description: foundTask.description,
              date: foundTask.date,
            }
          )
          .then((resp) => {
            if (resp.modifiedCount > 0) {
              return { resp: "ok", msg: `updated ${foundTask.name} task` };
            }
          });
      } else {
        return db
          .collection("tasks")
          .insertOne({
            board: task.board,
            name: task.name,
            date: task.date,
            description: task.description,
          })
          .then(() => {
            return { resp: "ok", msg: `inserted ${task.name} task` };
          })
          .catch((err) => {
            return { resp: "error", msg: "Error inserting task: " + err };
          });
      }
    })
    .catch((err) => {
      return { resp: "error", msg: "Error finding task: " + err };
    });
};

export { mongoDisconnect, mongoConnect, dbUrl, dbName, checkForNewUsers };
export { createRoom, addMessage, findRoom, checkUserChats };
export { getUsers, retrieveChatRooms, joinroom, retrieveChatMessages };
export { registerUser, loginUser, getAvailableUsers };
export { fetchTasks, updateTasks };
