import http from "http";
import express from "express";
import { fileURLToPath } from "node:url";
import { dirname, join } from "node:path";
import cors from "cors";
import { Server } from "socket.io";
import {
  mongoDisconnect,
  mongoConnect,
  dbUrl,
  dbName,
  checkForNewUsers,
} from "../database/database.js";

import { createRoom, addMessage } from "../database/database.js";
import {
  getUsers,
  retrieveChatRooms,
  joinroom,
  findRoom,
  checkUserChats,
  getAvailableUsers,
} from "../database/database.js";
import { loginUser, registerUser } from "../database/database.js";
import { fetchTasks, updateTasks } from "../database/database.js";

const app = express();
const server = http.createServer(app);
const io = new Server(server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"],
  },
});
const _dirname = dirname(fileURLToPath(import.meta.url));
app.get("/", (req, res) => {
  //console.log(_dirname);
  res.sendFile(join(_dirname, "/messages.html"));
});

async function fetchUsersFromSQL() {
  const hostname = "localhost";
  const port = 80;
  const phpFilePath = "/database/students.php";
  const apiUrl = `http://${hostname}:${port}${phpFilePath}`;
  await fetch(apiUrl)
    .then((response) => {
      if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
      }
      return response.json();
    })
    .then((users) => {
      //console.log("Response from server:", users);
      const usersJSON = users.map((user) => {
        const username = `${user.first_name} ${user.last_name}`;
        const password = user.id;
        return { username, password };
      });
      checkForNewUsers({ users: usersJSON });
    })
    .catch((error) => {
      console.error("Error fetching students:", error);
    });
}

app.use(cors({ origin: "*" }));
const rooms = {
  room: {
    users: {
      userId: {
        username: "",
        messages: [{ content: "", time: "" }],
      },
    },
  },
};
//let db;
mongoConnect(dbUrl, dbName, (_db, client) => {
  //db = _db;
  fetchUsersFromSQL();
});
io.on("connection", (socket) => {
  socket.on("request-rooms", async (user) => {
    try {
      const rooms = await retrieveChatRooms(user);
      //console.log("Retrieved chat rooms:", rooms);
      socket.emit("rooms-response", rooms);
    } catch (error) {
      console.error("Error:", error);
    }
  });

  socket.on("update-users", async () => {
    try {
      const users = await getUsers();
      //console.log("Retrieved users:", users);
      socket.emit("db-users", users);
    } catch (error) {
      console.error("Error:", error);
    }
  });

  socket.on("create-room", async (room, users, user) => {
    console.log("creating room");
    //console.log(users);
    try {
      const resp = await createRoom(room, users);
      console.log(resp.res);
      console.log("New room: " + resp.room.roomname);

      const rooms = await retrieveChatRooms(user);
      console.log("Retrieved chat rooms:", rooms);
      //socket.to(resp.room.roomname).emit("rooms-response", rooms);
      socket.emit("rooms-response", rooms);
    } catch (error) {
      console.log("Error creating room " + error);
    }
  });
  socket.on("findRoom", async (roomname) => {
    const resp = await findRoom(roomname);
    //console.log(resp.res);
    socket.emit("found-room", resp);
  });

  socket.on("joinRoom", async (roomname, user) => {
    socket.join(roomname);
    //console.log(user.username);

    //const userId = socket.id;
    //if (!rooms[roomname]) {
    //  rooms[roomname] = {
    //    users: {},
    //  };
    //}
    //
    //// Create user object for the current user
    //const _user = {
    //  username: user.username,
    //  messages: [],
    //};

    // Add the user to the room's users object
    //rooms[room].users[userId] = _user;
    //socket.to(room.roomname).emit("user-connected", rooms[room].users[userId], room);//check later
    try {
      //console.log(roomname);
      const history = await joinroom(roomname);
      //console.log(history);
      //console.log(`message keys ${Object.keys(history[0])}`);
      history.forEach((msg) => {
        //console.log(
        //`message keys ${msg.content} ${msg.username} ${msg.timestamp}`
        //);
      });
      //console.log(`${user.username} conneted to ${roomname}`);
      io.to(roomname).emit("chat-history", history);
      const usersInChatResp = await getUsers(roomname);
      if (usersInChatResp.resp === "ok") {
        io.to(roomname).emit("chat-members", usersInChatResp.users);
      }
    } catch (err) {
      console.log(
        "Server: couldnt get chat history of room: " + roomname + err
      );
    }
  });
  socket.on("register user", async (user) => {
    //console.log(`Server: ${user.username} requests to sign up`);
    socket.join(user.username);
    const registrationResp = await registerUser(user);
    if (registrationResp.res == "ok") {
      //console.log(registrationResp.userId);
      io.to(user.username).emit("registred", registrationResp);
    }
    //console.log(registrationResp.msg);
  });
  socket.on("login", async (user) => {
    //console.log(`Server: ${user.username} requests to log in`);
    socket.join(user.username);
    const loginResp = await loginUser(user);
    if (loginResp.res == "ok") {
      const rooms = await retrieveChatRooms(user);
      console.log("Retrieved chat rooms:", rooms);
      //socket.to(resp.room.roomname).emit("rooms-response", rooms);
      io.to(user.username).emit("rooms-response", rooms);
      //console.log(loginResp.userId);
      io.to(user.username).emit("user-logged-in", loginResp.userId);
    }
    console.log(loginResp.msg);
  });

  socket.on("send-chat-message", async (roomname, _message, user) => {
    //const userId = socket.id;
    //const username = rooms[room].users[userId].username;
    console.log(user._id);
    const message = { content: _message, timestamp: new Date().toISOString() };
    try {
      const roomresp = await findRoom(roomname);
      console.log(roomresp.res);
      const room = roomresp.room;
      console.log("Adding message room " + room._id + " " + room.roomname);
      const result = await addMessage(
        room,
        message.content,
        message.timestamp,
        user
      );
      console.log(result);
      if (result.res == "error") {
        throw err;
      }
      io.to(room.roomname).emit("new-chat-message", {
        content: _message,
        username: user.username,
      });
    } catch (err) {
      console.error("Server: Error sending message:", err);
    }
    //rooms[room].users[userId].messages.push(message);
  });
  socket.on("disconnect", () => {
    console.log("User disconnected");
  });
  socket.on("fetch users", async (user, roomname) => {
    socket.join(user.username);
    const users = await getAvailableUsers(roomname);
    console.log(users);
    io.to(user.username).emit("avaiable-users", users);
  });

  socket.on("add-users", async (usernames, roomname) => {
    const findResp = await findRoom(roomname);
    if (findResp.res === "found room") {
      const roomId = findResp.room._id;
      usernames.forEach(
        async (username) => await checkUserChats(roomId, username)
      );
    }
  });
  socket.on("task change", (task, user) => {
    socket.join(user.userId);
    const taskId = task._id;
    console.log(taskId);
    updateTasks(task, taskId).then((changeResponse) => {
      if (changeResponse.resp == "ok") {
        console.log(changeResponse.msg);
        io.to(user.userId).emit("task changed");
      }
    });
  });
  socket.on("fetch tasks", (user) => {
    socket.join(user.userId);
    fetchTasks().then((fetchTasksResp) => {
      io.to(user.userId).emit("tasks", fetchTasksResp);
    });
  });
});
//function getUsers(socket) {
//  return Object.entries(rooms).reduce((users, [user, room]) => {
//    if (room.users[socket.id] != null) users.push(user);
//    return users;
//  }, []);
//}

const PORT = 3000;
server.listen(PORT, () => {
  console.log(`Server running on ${PORT}`);
});
