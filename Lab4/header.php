<header id="page-header">
      <div id="page-header-container">
        <span id="page-header-info-span">CMS</span>

        <div id="page-header-profile">
          <div id="page-header-notification" style="display: flex">
            <img
              src="img/bell.png"
              alt="notification"
              style="width: 25px; height: 25px"
            />
            <div class="notifcation-circle"></div>
            <div id="sub-messages-wrap">
              <div id="mail-box">
                <div class="new-message">
                  <div class="sender-profile">
                    <img
                      src="img/sender.png"
                      alt="sender-icon"
                      class="sender-img"
                    />
                    <div class="sender-name" style="color: black">Admin</div>
                  </div>
                  <div class="sender-message">
                    <div class="message-rectangle">AAA</div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div id="authorized-user">
            <div class="user-name">
              <div id="page-header-profile-logo">
                <img
                  src="img/user.png"
                  alt="user"
                  style="width: 25px; height: 25px"
                />
              </div>
              <span id="page-header-username">Arthur Husar</span>
            </div>
            <div id="user-options">
              <ul id="user-options-links" class="list-group">
                <li class="list-group-item">Profile</li>
                <li class="list-group-item">Log out</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </header>