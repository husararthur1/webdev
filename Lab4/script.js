const addStudent = $(".add-student-container");
const addStudentForm = $("#add-student-form");
const removeWarning = $(".remove-student-warning-container");
const btnCloseAddStudent = $("#close-student-form");
const btnAddNewStudent = $("#add-new-student-button");
const overlay = $(".overlay");
const pages = $(".pagination .a-pag");

const pagPrevious = $(".pagination .a-pag-previous");
const pagNext = $(".pagination a-pag-next");

let studentTable = $("#students-table");
const warningMsg = $("#warning-message");

let currentContext = "add";

const btnAddStudentCreate = $("#add-student-create-button");

const mainCheckBox = $("#main-checkbox");

let studentData = {
  student_group: " ",
  first_name: " ",
  last_name: " ",
  gender: " ",
  birthday: {
    day: " ",
    month: " ",
    year: " ",
  },
  status: "online",
};

class Student {
  static count = 0;

  constructor(data) {
    this.group = data.student_group || "";
    this.firstName = data.first_name || "";
    this.lastName = data.last_name || "";
    this.gender = data.gender || "";
    this.birthday = data.birthday || "";
    this.status = data.status || "offline";
    this.id = this._generateStudentId();
    this.mode = "add";
    Student.count++;
  }

  _generateStudentId() {
    const { year, month, day } = this.birthday;
    const count = Student.count.toString().padStart(3, "0");

    return `${this.lastName.substring(0, 3).toUpperCase()}${this.firstName
      .substring(0, 1)
      .toUpperCase()}${year}${month}${day}${count}`;
  }
  init(studentData) {
    this.group = studentData.student_group;
    this.firstName = studentData.first_name;
    this.lastName = studentData.last_name;
    this.gender = studentData.gender;
    this.birthday = studentData.birthday;
    this.status = studentData.status;
    this.mode = currentContext;
  }
}

let students = [];

students.push(new Student(studentData));
const tbody = $("tbody");
let stToEdit = students[0];
students.pop();

function fillStudnetTable(studentsData) {
  studentsData.forEach((student) => {
    addNewStudent(student);
  });
}

$(document).ready(function () {
  $.ajax({
    url: "students.php",
    type: "GET",
    success: function (data) {
      console.log("Response from server:", data);
      const studentsData = JSON.parse(data);
      fillStudnetTable(studentsData);
    },
    error: function (xhr, status, error) {
      console.error("Error fetching students:", error);
    },
  });
});

btnAddNewStudent.on("click", function () {
  currentContext = "add";
  switchToCurrContext(currentContext);
  overlay.toggleClass("hidden");
  addStudent.toggleClass("hidden");
});

btnCloseAddStudent.on("click", function (e) {
  e.preventDefault();
  openCloseModal(addStudent);
});

const clearFormFields = function () {
  $("#group-select").val($("#group-default-option").val());
  $("#first-name-input").val("");
  $("#last-name-input").val("");
  $("#gender-select").val($("#gender-default-option").val());
  $("#birthday-input").val("");
};

const initStudent = function () {
  const std = {};
  std.student_group = $("#group-select").val();
  std.first_name = $("#first-name-input").val();
  std.last_name = $("#last-name-input").val();
  std.gender = $("#gender-select").val();
  std.birthday = $("#birthday-input").val();
  std.status = "online";
  std.mode = currentContext;
  return std;
};

const fillEditForm = function (student) {
  $("#group-select").val(student.group);
  $("#first-name-input").val(student.firstName);
  $("#last-name-input").val(student.lastName);
  $("#gender-select").val(student.gender);
  let [year, month, day] = student.birthday.split("-");
  $("#birthday-input").val(`${day}-${month}-${year}`);
};

const editRow = function (response) {
  const student = parseResponse(response);
  $(rowToEdit).find("td:nth-child(2)").text(student.group);
  $(rowToEdit)
    .find("td:nth-child(3)")
    .text(`${student.firstName} ${student.lastName}`);
  $(rowToEdit).find("td:nth-child(4)").text(student.gender);
  const [year, month, day] = student.birthday.split("-");

  $(rowToEdit).find("td:nth-child(5)").text(`${day}.${month}.${year}`);

  $(rowToEdit).find("td:nth-child(6)").toggleClass(`${student.status}`);
  //for (let i = 0; i < students.length; i++) {
  //  if (students[i].id === student.id) {
  //    students[i] = student;
  //  }
  //}
};

function readFromTable(tr) {
  const student = {};
  const [year, month, day] = $(tr).find("td:nth-child(5)").text().split(".");
  student.group = $(tr).find("td:nth-child(2)").text();
  [student.firstName, student.lastName] = $(tr)
    .find("td:nth-child(3)")
    .text()
    .split(" ");
  student.gender = $(tr).find("td:nth-child(4)").text();
  student.birthday = `${year}-${month}-${day}`;
  student.id = +$(tr).attr("id");
  student.mode = currentContext;
  return student;
}

function addStudentToTable(student) {
  const tr = $("<tr></tr>");
  const checkboxCell = $("<td></td>");
  const checkboxInput = $("<input>");
  $(checkboxInput).attr("type", "checkbox");
  $(checkboxInput).addClass("secondary-checkbox");
  $(checkboxCell).append(checkboxInput);
  $(tr).append(checkboxCell);

  const groupCell = $("<td></td>");
  $(groupCell).text(student.group);
  $(tr).append(groupCell);

  const nameCell = $("<td></td>");
  $(nameCell).text(`${student.firstName} ${student.lastName}`);
  $(tr).append(nameCell);

  const genderCell = $("<td></td>");
  $(genderCell).text(student.gender);
  $(tr).append(genderCell);
  const [year, month, day] = student.birthday.split("-");
  const birthDayCell = $("<td></td>");
  $(birthDayCell).text(`${day}.${month}.${year}`);
  $(tr).append(birthDayCell);

  const statusCell = $("<td></td>");
  const statusC = $("<div></div>");
  $(statusC).addClass("student-status online");
  $(statusCell).append(statusC);
  $(tr).append(statusCell);

  const optionButtonsCell = $("<td></td>");
  const optionButtons = $("<div></div>");
  $(optionButtons).addClass("option-buttons");

  const editButtonC = $("<div></div>");
  $(editButtonC).addClass("edit-student");
  const editButton = $("<button></button>");
  $(editButton).addClass("edit-student-button");
  $(editButton).addClass("btn");
  $(editButton).addClass("btn-outline-success");
  const editImg = $("<img>");
  $(editImg).attr("src", "img/edit.png");
  $(editImg).attr("style", "width: 10px; height: 10px");
  $(editImg).attr("alt", "edit");

  $(editButton).append(editImg);
  $(editButtonC).append(editButton);
  $(optionButtons).append(editButtonC);

  const removeButtonC = $("<div></div>");
  $(removeButtonC).addClass("remove-student");
  const removeButton = $("<button></button>");
  $(removeButton).addClass("remove-student-button");
  $(removeButton).addClass("btn");
  $(removeButton).addClass("btn-outline-danger");
  const removeImg = $("<img>");
  $(removeImg).attr("src", "img/close.png");
  $(removeImg).attr("style", "width: 10px; height: 10px");
  $(removeImg).attr("alt", "close");

  $(removeButton).append(removeImg);
  $(removeButtonC).append(removeButton);
  $(optionButtons).append(removeButtonC);
  $(optionButtonsCell).append(optionButtons);
  $(tr).attr("id", student.id);
  $(tr).append(optionButtonsCell);
  $(tbody).append(tr);
}

function parseResponse(response) {
  const student = {};

  student.group = response.student_group;
  student.firstName = response.first_name;
  student.lastName = response.last_name;
  student.gender = response.gender;
  student.birthday = response.birthday;
  student.status = response.status;
  student.id = response.id;
  student.mode = currentContext;
  return student;
}

function addNewStudent(response) {
  const student = parseResponse(response);
  addStudentToTable(student);
}

function openCloseModal(modal) {
  overlay.toggleClass("hidden");
  modal.toggleClass("hidden");
  clearFormFields();
}

addStudentForm.submit(function (e) {
  const form = document.getElementById("add-student-form");
  const formIsValid = form.checkValidity();
  e.preventDefault();
  e.stopPropagation();
  form.classList.add("was-validated");

  if (formIsValid) {
    const student = initStudent(); //stToEdit.init(initStudent());
    sendDataToServer(student);
  }
});

const btnRemoveStudent = $(".remove-student-button");
const btnConfirmRemoval = $("#confirm-remove-button");
const btnCancelRemoval = $("#cancel-remove-button");
const btnCloseWarning = $("#warning-close-button");

let rowToRemove, rowToEdit;
const switchToCurrContext = function () {
  const info = $("#add-student-header-caption");
  if (currentContext === "edit") {
    info.text("Edit Student");
    btnAddStudentCreate.text("Save");
  } else {
    info.text("Add Student");
    btnAddStudentCreate.text("Create");
  }
};

btnConfirmRemoval.on("click", function () {
  removeStudentFromServer();
  openCloseModal(removeWarning);
});

function removeStudentFromServer() {
  const id = rowToRemove.attr("id");
  const idJSON = JSON.stringify(id);
  console.log(idJSON);
  $.ajax({
    type: "POST",
    url: "remove_student.php",
    data: idJSON,
    success: function (resp) {
      const response = JSON.parse(resp);
      if (response.error) {
        console.error("Server error:", response.error);
        $("#errorMessage").text("Server error: " + response.error);
        $("#errorMessage").removeClass("hidden");
      } else {
        console.log("Received ", response);
        rowToRemove.remove();
        $("#errorMessage").addClass("hidden");
      }
    },
    error: function (xhr, status, error) {
      console.error("Failed. Error :", error);
      $("#errorMessage").text("Server error: " + error);
      $("#errorMessage").toggleClass("hidden");
    },
  });
}

studentTable.on("click", function (e) {
  if (
    e.target &&
    ($(e.target).hasClass("remove-student-button") ||
      $(e.target).parent().hasClass("remove-student-button"))
  ) {
    rowToRemove = $(e.target).closest("tr");

    warningMsg.text(
      `Are you sure you want to remove \n${$(rowToRemove)
        .find("td:nth-child(3)")
        .text()}?`
    );
    openCloseModal(removeWarning);
  }

  if ($(e.target).is("#main-checkbox")) {
    $(".secondary-checkbox").prop("checked", e.target.checked);
  }
  if (
    e.target &&
    ($(e.target).hasClass("edit-student-button") ||
      $(e.target).parent().hasClass("edit-student-button"))
  ) {
    rowToEdit = $(e.target).closest("tr");
    openCloseModal(addStudent);
    currentContext = "edit";
    switchToCurrContext();
    const student = readFromTable(rowToEdit);
    fillEditForm(student);
  }
});

btnCancelRemoval.on("click", function () {
  overlay.addClass("hidden");
  removeWarning.addClass("hidden");
});

btnCloseWarning.on("click", function () {
  overlay.addClass("hidden");
  removeWarning.addClass("hidden");
});

// pagNext.on("click", function () {
//   let current = document.querySelector(".active");
//   let currentIndex = Array.from(pages).indexOf(current);

//   if (currentIndex === pages.length - 1) {
//     pages[0].classList.add("active");
//   } else {
//     pages[currentIndex + 1].classList.add("active");
//   }

//   if (current) {
//     current.classList.remove("active");
//   }
// });
pagPrevious.on("click", function () {
  let current = $(".active");
  let currentIndex = pages.index(current);

  if (currentIndex <= 0) {
    pages.eq(pages.length - 1).addClass("active");
  } else {
    pages.eq(currentIndex - 1).addClass("active");
  }

  if (current) {
    current.removeClass("active");
  }
});

pagNext.on("click", function () {
  // console.log("pagNext click");
  let current = $(".active");
  let currentIndex = pages.index(current);

  if (currentIndex === pages.length - 1) {
    pages.eq(0).addClass("active");
  } else {
    pages.eq(currentIndex + 1).addClass("active");
  }

  if (current) {
    current.removeClass("active");
  }
});
pages.each(function () {
  $(this).on("click", function () {
    // console.log("pages.each click");
    $(".active").removeClass("active");
    $(this).addClass("active");
  });
});

function sendDataToServer(student) {
  const studentsJSON = JSON.stringify({
    id: rowToEdit ? rowToEdit.attr("id") : -1,
    ...student,
  });
  console.log(studentsJSON);
  $.ajax({
    type: "POST",
    url: "validation.php",
    data: studentsJSON,
    success: function (resp) {
      const response = JSON.parse(resp);
      if (response.error) {
        console.error("Server error:", response.error);
        $("#errorMessage").text("Server error: " + response.error);
        $("#errorMessage").removeClass("hidden");
      } else {
        console.log("Received ", response);
        if (currentContext === "add") {
          addNewStudent(response);
        } else if (currentContext === "edit") {
          editRow(response);
        }
        $("#errorMessage").addClass("hidden");
        openCloseModal(addStudent);
      }
    },
    error: function (xhr, status, error) {
      console.error("Failed. Error :", error);
      $("#errorMessage").text("Server error: " + error);
      $("#errorMessage").toggleClass("hidden");
    },
  });
}
