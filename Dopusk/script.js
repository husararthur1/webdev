const p = document.getElementById("italic-column");
const btnFetch = document.getElementById("fetchWords");
const btnCorrect = document.getElementById("correctWords");

const wordListDiv = document.querySelector(".words-list");
btnFetch.addEventListener("click", function () {
  wordListDiv.innerHTML = ``;

  const wordList = document.createElement("ul");
  wordListDiv.appendChild(wordList);

  const italicWords = document.querySelectorAll("i");
  italicWords.forEach((word) => {
    const wordLI = document.createElement("li");
    wordLI.textContent = word.textContent;
    const checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    checkbox.addEventListener("click", function () {
      if (this.checked) {
        wordLI.style.textDecoration = "line-through";
      } else {
        wordLI.style.textDecoration = "none";
      }
    });

    wordLI.append(checkbox);
    wordList.appendChild(wordLI);
  });
});
