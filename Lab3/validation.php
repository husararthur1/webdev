<?php 
    $jsonData = file_get_contents('php://input');
    $decodedData = json_decode($jsonData, true);
    

    if($decodedData) {
        $group = $decodedData['group'];
        $firstName = $decodedData['firstName'];
        $lastName = $decodedData['lastName'];
        $gender = $decodedData['gender'];
        $birthDayYear = $decodedData['birthDay']['year'];
        $birthDayMonth = $decodedData['birthDay']['month'];
        $birthDayDay = $decodedData['birthDay']['day'];
        $status = $decodedData['status'];
        $id = $decodedData['id'];
        

        $response = array();

        if (!ctype_alpha($firstName)) {
            $response['error'] = 'Error: First Name of student must contain only characters';
        }
        if (!ctype_alpha($lastName)) {
            $response['error'] = 'Error: Last Name of student must contain only characters';
        }
        if ($birthDayYear > 2006) {
            $response['error'] = 'Error: Student must be at least 16 y.o';
        }

        // If there's no error, set the student data in the response
        if (empty($response)) {
            $response = array(
                'group' => $group,
                'firstName' => $firstName,
                'lastName' => $lastName,
                'gender' => $gender,
                'birthDay' => array(
                    'year' => $birthDayYear,
                    'month' => $birthDayMonth,
                    'day' => $birthDayDay
                ),
                'status' => $status,
                'id' => $id
            );
        }


        $jsonResponse = json_encode($response);
        echo $jsonResponse;
        //echo "Group: $group, Name: $firstName $lastName, Gender: $gender, Birthday: $birthDayYear-$birthDayMonth-$birthDayDay, Status: $status, ID: $id";
    } else {
        echo array('error' => "Error: JSON data not received");
    }
?>