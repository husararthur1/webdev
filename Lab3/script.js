const addStudent = $(".add-student-container");
const addStudentForm = $("#add-student-form");
const removeWarning = $(".remove-student-warning-container");
const btnCloseAddStudent = $("#close-student-form");
const btnAddNewStudent = $("#add-new-student-button");
const overlay = $(".overlay");
const pages = $(".pagination .a-pag");

const pagPrevious = $(".pagination .a-pag-previous");
const pagNext = $(".pagination a-pag-next");

let studentTable = $("#students-table");
const warningMsg = $("#warning-message");

btnAddNewStudent.on("click", function () {
  currentContext = "add";
  switchToCurrContext(currentContext);
  overlay.toggleClass("hidden");
  addStudent.toggleClass("hidden");
});

btnCloseAddStudent.on("click", function (e) {
  e.preventDefault();
  $("#errorMessage").addClass("hidden");
  openCloseModal(addStudent);
});

let currentContext = "add";

const btnAddStudentCreate = $("#add-student-create-button");

const mainCheckBox = $("#main-checkbox");

let studentData = {
  group: " ",
  firstName: " ",
  lastName: " ",
  gender: " ",
  birthDay: {
    day: " ",
    month: " ",
    year: " ",
  },
  status: "online",
};

class Student {
  static count = 0;

  constructor(data) {
    this.group = data.group || "";
    this.firstName = data.firstName || "";
    this.lastName = data.lastName || "";
    this.gender = data.gender || "";
    this.birthDay = {
      year: data.birthDay.year || "",
      month: data.birthDay.month || "",
      day: data.birthDay.day || "",
    };
    this.status = data.status || "offline";
    this.id = this._generateStudentId();
    Student.count++;
  }

  _generateStudentId() {
    const { year, month, day } = this.birthDay;
    const count = Student.count.toString().padStart(3, "0");

    return `${this.lastName.substring(0, 3).toUpperCase()}${this.firstName
      .substring(0, 1)
      .toUpperCase()}${year}${month}${day}${count}`;
  }
  init(studentData) {
    this.group = studentData.group;
    this.firstName = studentData.firstName;
    this.lastName = studentData.lastName;
    this.gender = studentData.gender;
    this.birthDay = studentData.birthDay;
    this.status = studentData.status;
  }
}

let students = [];

students.push(new Student(studentData));
const tbody = $("tbody");
let stToEdit = students[0];
students.pop();
const clearFormFields = function () {
  $("#group-select").val($("#group-default-option").val());
  $("#first-name-input").val("");
  $("#last-name-input").val("");
  $("#gender-select").val($("#gender-default-option").val());
  $("#birthday-input").val("");
};

const initStudent = function () {
  let std = { birthDay: {} };
  std.group = $("#group-select").val();
  std.firstName = $("#first-name-input").val();
  std.lastName = $("#last-name-input").val();
  std.gender = $("#gender-select").val();

  const [year, month, day] = $("#birthday-input").val().split("-");
  std.birthDay["year"] = year;
  std.birthDay["month"] = month;
  std.birthDay["day"] = day;
  std.status = "online";
  return std;
};

const fillEditForm = function (studentData) {
  $("#group-select").val(studentData.group);
  $("#first-name-input").val(studentData.firstName);
  $("#last-name-input").val(studentData.lastName);
  $("#gender-select").val(studentData.gender);
  $("#birthday-input").val(
    studentData.birthDay["year"] +
      "-" +
      studentData.birthDay["month"] +
      "-" +
      studentData.birthDay["day"]
  );
};

const editRow = function (student) {
  student.init(initStudent());
  $(rowToEdit).find("td:nth-child(2)").text(student.group);
  $(rowToEdit)
    .find("td:nth-child(3)")
    .text(`${student.firstName} ${student.lastName}`);
  $(rowToEdit).find("td:nth-child(4)").text(student.gender);

  $(rowToEdit)
    .find("td:nth-child(5)")
    .text(
      `${student.birthDay.day}.${student.birthDay.month}.${student.birthDay.year}`
    );

  $(rowToEdit).find("td:nth-child(6)").toggleClass(`${student.status}`);
  for (let i = 0; i < students.length; i++) {
    if (students[i].id === student.id) {
      students[i] = student;
    }
  }
};

function checkStudentData(student) {}

function addStudentToTable(student) {
  const tr = $("<tr></tr>");
  const checkboxCell = $("<td></td>");
  const checkboxInput = $("<input>");
  $(checkboxInput).attr("type", "checkbox");
  $(checkboxInput).addClass("secondary-checkbox");
  $(checkboxCell).append(checkboxInput);
  $(tr).append(checkboxCell);

  const groupCell = $("<td></td>");
  $(groupCell).text(student.group);
  $(tr).append(groupCell);

  const nameCell = $("<td></td>");
  $(nameCell).text(`${student.firstName} ${student.lastName}`);
  $(tr).append(nameCell);

  const genderCell = $("<td></td>");
  $(genderCell).text(student.gender);
  $(tr).append(genderCell);

  const birthDayCell = $("<td></td>");
  $(birthDayCell).text(
    `${student.birthDay.day}.${student.birthDay.month}.${student.birthDay.year}`
  );
  $(tr).append(birthDayCell);

  const statusCell = $("<td></td>");
  const statusC = $("<div></div>");
  $(statusC).addClass("student-status online");
  $(statusCell).append(statusC);
  $(tr).append(statusCell);

  const optionButtonsCell = $("<td></td>");
  const optionButtons = $("<div></div>");
  $(optionButtons).addClass("option-buttons");

  const editButtonC = $("<div></div>");
  $(editButtonC).addClass("edit-student");
  const editButton = $("<button></button>");
  $(editButton).addClass("edit-student-button");
  $(editButton).addClass("btn");
  $(editButton).addClass("btn-outline-success");
  const editImg = $("<img>");
  $(editImg).attr("src", "img/edit.png");
  $(editImg).attr("style", "width: 10px; height: 10px");
  $(editImg).attr("alt", "edit");

  $(editButton).append(editImg);
  $(editButtonC).append(editButton);
  $(optionButtons).append(editButtonC);

  const removeButtonC = $("<div></div>");
  $(removeButtonC).addClass("remove-student");
  const removeButton = $("<button></button>");
  $(removeButton).addClass("remove-student-button");
  $(removeButton).addClass("btn");
  $(removeButton).addClass("btn-outline-danger");
  const removeImg = $("<img>");
  $(removeImg).attr("src", "img/close.png");
  $(removeImg).attr("style", "width: 10px; height: 10px");
  $(removeImg).attr("alt", "close");

  $(removeButton).append(removeImg);
  $(removeButtonC).append(removeButton);
  $(optionButtons).append(removeButtonC);
  $(optionButtonsCell).append(optionButtons);
  $(tr).attr("id", student.id);
  $(tr).append(optionButtonsCell);
  $(tbody).append(tr);
}

function addNewStudent() {
  studentData = initStudent();

  const student = new Student(studentData);
  students.push(student);
  addStudentToTable(student);
  clearFormFields();
  return student;
}

function openCloseModal(modal) {
  overlay.toggleClass("hidden");
  modal.toggleClass("hidden");
}

addStudentForm.submit(function (e) {
  const form = document.getElementById("add-student-form");
  const formIsValid = form.checkValidity();
  e.preventDefault();
  e.stopPropagation();
  form.classList.add("was-validated");

  if (formIsValid) {
    stToEdit.init(initStudent());
    sendDataToServer(stToEdit);
  }
});

const btnRemoveStudent = $(".remove-student-button");
const btnConfirmRemoval = $("#confirm-remove-button");
const btnCancelRemoval = $("#cancel-remove-button");
const btnCloseWarning = $("#warning-close-button");

let rowToRemove, rowToEdit;
const switchToCurrContext = function () {
  const info = $("#add-student-header-caption");
  if (currentContext === "edit") {
    info.text("Edit Student");
    btnAddStudentCreate.text("Save");
  } else {
    info.text("Add Student");
    btnAddStudentCreate.text("Create");
  }
};

btnConfirmRemoval.on("click", function () {
  const id = rowToRemove.attr("id");
  const stRemove = students.find((st) => st.id === id);

  students.splice(students.indexOf(stRemove), 1);
  rowToRemove.remove();
  openCloseModal(removeWarning);
});

studentTable.on("click", function (e) {
  if (
    e.target &&
    ($(e.target).hasClass("remove-student-button") ||
      $(e.target).parent().hasClass("remove-student-button"))
  ) {
    rowToRemove = $(e.target).closest("tr");

    warningMsg.text(
      `Are you sure you want to remove \n${$(rowToRemove)
        .find("td:nth-child(3)")
        .text()}?`
    );
    openCloseModal(removeWarning);
  }

  if ($(e.target).is("#main-checkbox")) {
    $(".secondary-checkbox").prop("checked", e.target.checked);
  }
  if (
    e.target &&
    ($(e.target).hasClass("edit-student-button") ||
      $(e.target).parent().hasClass("edit-student-button"))
  ) {
    rowToEdit = $(e.target).closest("tr");
    openCloseModal(addStudent);
    currentContext = "edit";
    switchToCurrContext();
    stToEdit = students.find((st) => st.id === rowToEdit.attr("id"));
    fillEditForm(stToEdit);
  }
});

btnCancelRemoval.on("click", function () {
  overlay.addClass("hidden");
  removeWarning.addClass("hidden");
});

btnCloseWarning.on("click", function () {
  overlay.addClass("hidden");
  removeWarning.addClass("hidden");
});

// pagNext.on("click", function () {
//   let current = document.querySelector(".active");
//   let currentIndex = Array.from(pages).indexOf(current);

//   if (currentIndex === pages.length - 1) {
//     pages[0].classList.add("active");
//   } else {
//     pages[currentIndex + 1].classList.add("active");
//   }

//   if (current) {
//     current.classList.remove("active");
//   }
// });
pagPrevious.on("click", function () {
  let current = $(".active");
  let currentIndex = pages.index(current);

  if (currentIndex <= 0) {
    pages.eq(pages.length - 1).addClass("active");
  } else {
    pages.eq(currentIndex - 1).addClass("active");
  }

  if (current) {
    current.removeClass("active");
  }
});

pagNext.on("click", function () {
  let current = $(".active");
  let currentIndex = pages.index(current);

  if (currentIndex === pages.length - 1) {
    pages.eq(0).addClass("active");
  } else {
    pages.eq(currentIndex + 1).addClass("active");
  }

  if (current) {
    current.removeClass("active");
  }
});
pages.each(function () {
  $(this).on("click", function () {
    $(".active").removeClass("active");
    $(this).addClass("active");
  });
});

function sendDataToServer(student) {
  const studentsJSON = JSON.stringify(student);
  console.log("Sent:", studentsJSON);

  $.ajax({
    type: "POST",
    url: "validation.php",
    data: studentsJSON,
    success: function (resp) {
      const response = JSON.parse(resp);
      if (response.error) {
        console.error("Server error:", response.error);
        $("#errorMessage").text(response.error);
        $("#errorMessage").removeClass("hidden");
      } else {
        console.log("Received ", response);
        if (currentContext === "add") {
          stToEdit = addNewStudent();
        } else if (currentContext === "edit") {
          editRow(stToEdit);
        }
        $("#errorMessage").addClass("hidden");
        openCloseModal(addStudent);
      }
    },
    error: function (xhr, status, error) {
      console.error("Failed. Error :", error);
      $("#errorMessage").text("Server error: " + error);
      $("#errorMessage").toggleClass("hidden");
    },
  });
}
